Entregables:
1. El documento de planeación del sitio Web actualizado: [plan_proyecto.pdf](https://gitlab.com/carloscerlira/pcswe-proyecto-final/-/blob/master/plan_proyecto.pdf)
2. La guía de estilos conforme a lo trabajado en el módulo #3 "Principios para el diseño 
de interfaces de usuario": [guía_estilos.pdf](https://gitlab.com/carloscerlira/pcswe-proyecto-final/-/blob/master/guía_estilos.pdf)
3. El código fuente completo del sitio Web (PHP, JS, HTML, CSS) incluyendo los 
archivos editables en formato PSD de las imágenes creadas o modificadas para el 
sitio: dentro del directorio [public_html](https://gitlab.com/carloscerlira/pcswe-proyecto-final/-/tree/master/public_html)
4. Scripts SQL para la creación o restauración de la base de datos: [create_database.sql](https://gitlab.com/carloscerlira/pcswe-proyecto-final/-/blob/master/create_database.sql)
5. Documento con las instrucciones, paso a paso para instalar el sitio Web. Al menos 
debe incluir la instalación de la base de datos y del código fuente para hacer 
funcionar el sitio Web: [INSTALL.md](https://gitlab.com/carloscerlira/pcswe-proyecto-final/-/blob/master/INSTALL.md)
6. Un archivo de texto [LEEME.txt](https://gitlab.com/carloscerlira/pcswe-proyecto-final/-/blob/master/LEEME.txt) con los siguientes datos:
    - Nombre completo del alumno
    - Nombre del proyecto
    - Correo electrónico del alumno
    - Dirección electrónica de su proyecto, alojado en servidores de DGTIC, no se 
aceptarán proyectos alojados en servidores externos.
e. 
    - Nombres de usuario y contraseña en caso de que el sitio Web tenga algún 
sistema de autenticación o cualquier otro dato necesario para la revisión.
7. Deberán dar acceso a su repositorio de gitlab a las cuentas:  
user: @comando132, email: dev.hugo.cuellar@gmail.com  
user: @ana, email: anici@comunidad.unam.mx