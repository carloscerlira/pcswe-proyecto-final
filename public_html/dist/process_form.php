<?php

$hostname = "localhost";
$username = "dbcuenta05";
$password = "Cta.m1sql05";
$dbname = "db05";

try {
  $conn = mysqli_connect(hostname: $hostname, 
                        username: $username,
                        password: $password,
                        database: $dbname);
  // echo "Connected successfully";
} catch(PDOException $e) {
  // echo "Connection failed: " . $e->getMessage();
}

$name = $_POST["name"];
$email = $_POST["email"];
$phone = $_POST["phone"];
$message = $_POST["message"];

// var_dump($name, $email, $phone, $message);

$sql = "INSERT INTO message (name, email, phone, message) 
        VALUES (?, ?, ?, ?)";

$stmt = mysqli_stmt_init($conn);

if(!mysqli_stmt_prepare($stmt, $sql)){
  echo "error";
  die(mysqli_error($conn));
}

mysqli_stmt_bind_param($stmt, "ssss",
$name,
$email,
$phone,
$message);

mysqli_stmt_execute($stmt);
echo "success";

