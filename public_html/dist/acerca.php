<?php
    include('connection.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Club de Programación Competitiva de la Facultad de Ingeniería UNAM. Encuentra concursos, material de preparación, cursos y eventos relacionados a programación competitiva" />
        <meta name="author" content="CPCFI" />
        <title>Club de Programación Competitiva de la Facultad de Ingeniería UNAM</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/logos/logo_cpcfi_2.png" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" /> -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="index.php"><img src="assets/img/logos/logo_letras.png" alt="..." style="width: 130px; height: auto"/></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ms-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                        <li class="nav-item"><a class="nav-link" href="index.php#services">Inicio</a></li>
                        <li class="nav-item"><a class="nav-link" href="acerca.php#about">Acerca de</a></li>
                        <li class="nav-item"><a class="nav-link" href="concursos.php#concurso_cpcfi">Concurso CPCFI 2022</a></li>
                        <li class="nav-item"><a class="nav-link" href="recursos.php#recursos">Recursos</a></li>
                        <li class="nav-item"><a class="nav-link" href="contacto.php#redes">Contacto</a></li>
                        <?php if (isset($user)): ?>
                            <li class="nav-item"><a class="nav-link" href="logout.php"><?= htmlspecialchars($user["name"])?></a></li>
                        <?php else: ?>
                            <li class="nav-item"><a class="nav-link" href="login.php">Login</a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <img src="assets/img/logos/logo_cpcfi_3.png" style="width: 300px;"></img>                
                <!-- <div class="masthead-heading text-uppercase">CPCFI</div> -->
                <div class="masthead-subheading">Club de Programación Competitiva de la Facultad de Ingeniería, UNAM</div>
                <!-- <a class="btn btn-primary btn-xl text-uppercase" href="#services">Tell Me More</a> -->
            </div>
        </header>

        <!-- About-->
        <section class="page-section" id="about">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Acerca de</h2>
                    <!-- <h3 class="section-subheading text-muted">Descubre qué es el CPCFI</h3> -->
                </div>
                <div class="container" style="margin-top: 30px;">
                    <div class="row text-center">
                        <div class="col" style="text-align: left;">
                            <h2>¿Quíenes somos?</h2>
                            <p>
                                Somos un equipo de estudiantes, profesores y ex-alumnos de la Faculta de Ingeniería y de la UNAM que buscá crear una comunidad apasionada por aprender programación competitiva y participar 
                                en concursos nacionales e internacionales representando a México y a la Universidad.
                            </br>
                                Compartimos la pasión por programar, resolver problemas y trabajar en equipo. Así mismo, nos gusta compartir nuestro conocimiento y ayudar a los demás a aprender.
                            </p>            
                            <h2>¿Qué hacemos?</h2>
                            <p>
                                Tenemos tres grandes objetivos:
                            </p>
                            <ul>
                                <li>Formar y entrenar equipos que representen a la Faculta de Ingeniería y a la UNAM en el <a href="icpc.php#icpc">International Collegiate Programming Contest (ICPC)</a>.</li>
                                <li>Crear eventos que fomenten la programación, siendo el principal de estos nuestro <a href="concursos.php#concurso_cpcfi">Concurso Anual de Programación Competitiva CPCFI</a>.</li>
                                <li>Fomentar la vinculación entre empresas y alumnos. Nuestros integrantes han realizado internships en empresas como Facebook, Microsoft, Google, etc. </li>
                            </ul>
                            <h2>¿Cómo unirse?</h2>
                            <p>
                                Para ser parte del club solo hace falta participar en nuestros eventos y seguirnos en nuestras <a href="contacto.php#redes">redes sociales</a>. Si estas interesado en recibir entrenamiento 
                                por parte de nuestros profesores, puedes esperar a nuestra convocatoria que se pública cada verano o puedes <a href="contacto.php#contact">enviarnos un mensaje</a>.
                            </p>
                            <h2>Nuestra historia</h2>
                            <ul class="timeline">
                                <li>
                                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/logos/logo_cpcfi_2.png" alt="..." /></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4>Verano 2021</h4>
                                            <h4 class="subheading">Fundación del club</h4>
                                        </div>
                                        <div class="timeline-body"><p class="text-muted">El club fue fundado en verano de 2021 por el <a href="https://www.fi-b.unam.mx/info-pa.php?depto=computacion&nombre=JorgeSolano">M.C.I Jorge Solano</a>, profesor de la Facultad de Ingeniería y el estudiante Néstor Martínez</p></div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/logos/logo_icpc.png" alt="..." /></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4>Otoño 2021</h4>
                                            <h4 class="subheading">Primer generación</h4>
                                        </div>
                                        <div class="timeline-body"><p class="text-muted">La primer generación, conformada por 21 estudiantes de la Facultad de Ingeniería comenzo clases. <a href="https://icpc.global/regionals/finder/mcapgp-2021/standings">Nuestro mejor equipo obtuvo el lugar 23 en el ICPC 2022</a></p></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/CPCFI_FirstGen.jpg" alt="..." /></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4>Otoño 2022</h4>
                                            <h4 class="subheading">Segunda generación</h4>
                                        </div>
                                        <div class="timeline-body"><p class="text-muted">La segunda generación, confromada por 10 estudiantes de la Facultad de Ingeniería comenzo clases. Nuestro mejor equipo obutvo el lugar 14 en el ICPC 2023 Primer Fecha</p></div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/algorithms.png" alt="..." /></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4>Agosto 2022</h4>
                                            <h4 class="subheading">Primer concurso de programación</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p class="text-muted">
                                            El club organizo su primer <a href="concursos.php#concurso_cpcfi">Concruso Anual de Programación Competitiva CPCFI 2022</a> como parte del festejo de 45 años de la Licenciatura en Computación en colaboración con OmegaUP</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-image">
                                        <h4 style="color: black;">
                                            Se parte
                                            <br />
                                            de nuestra
                                            <br />
                                            historia!
                                        </h4>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <div id="footer-placeholder">
        </div>
        <script>
            $(function(){
              $("#footer-placeholder").load("footer.html");
            });
        </script>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>