<?php
    include('connection.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Club de Programación Competitiva de la Facultad de Ingeniería UNAM. Encuentra concursos, material de preparación, cursos y eventos relacionados a programación competitiva" />
        <meta name="author" content="CPCFI" />
        <title>Club de Programación Competitiva de la Facultad de Ingeniería UNAM</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/logos/logo_cpcfi_2.png" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" /> -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="index.php"><img src="assets/img/logos/logo_letras.png" alt="..." style="width: 130px; height: auto"/></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ms-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                        <li class="nav-item"><a class="nav-link" href="index.php">Inicio</a></li>
                        <li class="nav-item"><a class="nav-link" href="acerca.php#about">Acerca de</a></li>
                        <li class="nav-item"><a class="nav-link" href="concursos.php#concurso_cpcfi">Concurso CPCFI 2022</a></li>
                        <li class="nav-item"><a class="nav-link" href="recursos.php#recursos">Recursos</a></li>
                        <li class="nav-item"><a class="nav-link" href="contacto.php#redes">Contacto</a></li>
                        <?php if (isset($user)): ?>
                            <li class="nav-item"><a class="nav-link" href="logout.php"><?= htmlspecialchars($user["name"])?></a></li>
                        <?php else: ?>
                            <li class="nav-item"><a class="nav-link" href="login.php">Login</a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <img src="assets/img/logos/logo_cpcfi_3.png" style="width: 300px;"></img>                
                <!-- <div class="masthead-heading text-uppercase">CPCFI</div> -->
                <div class="masthead-subheading">Club de Programación Competitiva de la Facultad de Ingeniería, UNAM</div>
                <!-- <a class="btn btn-primary btn-xl text-uppercase" href="#services">Tell Me More</a> -->
            </div>
        </header>

        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v14.0&appId=342551817172049&autoLogAppEvents=1" nonce="ODl9fLIo"></script>
        <!-- Services-->
        <section class="page-section" id="services">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Información general</h2>
                    <h3 class="section-subheading text-muted">¿En qué estas interesado?</h3>
                </div>
                <div class="row text-center">
                    <div class="col-md-4">
                        <a href="acerca.php#about" style="color: inherit; text-decoration: none;">
                            <span class="fa-stack fa-4x">
                                <i class="fas fa-circle fa-stack-2x" style="color: #ffc83b;"></i>
                                <i class="fas fa-people-group fa-stack-1x fa-inverse"></i>
                            </span>
                            <h4 class="my-3">Acerca de</h4>
                        </a>
                        <p class="text-muted">Conoce nuestra historia, quíénes somos, qué hacemos y como únirte</p>
                    </div>
                    <div class="col-md-4">
                        <a href="concursos.php#concurso_cpcfi" style="color: inherit; text-decoration: none;">
                            <span class="fa-stack fa-4x">
                                <i class="fas fa-circle fa-stack-2x" style="color: #ff3a86;"></i>
                                <i class="fas fa-trophy fa-stack-1x fa-inverse"></i>
                            </span>
                            <h4 class="my-3">Concurso de Programación</h4>
                        </a>
                        <p class="text-muted">Participa en el concurso anual de programación competitiva organizado por el club</p>
                    </div>
                    <div class="col-md-4">
                        <a href="recursos.php#recursos" style="color: inherit; text-decoration: none;">
                            <span class="fa-stack fa-4x">
                                <i class="fas fa-circle fa-stack-2x" style="color: #bb1534;"></i>
                                <i class="fas fa-building-columns fa-stack-1x fa-inverse"></i>
                            </span>
                            <h4 class="my-3">Recursos</h4>
                        </a>
                        <p class="text-muted">Encuentra algoritmos y estructuras de datos explicados desde cero así como problemas de práctica</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- Portfolio Grid-->
        <section class="page-section bg-light" id="portfolio">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Eventos</h2>
                    <h3 class="section-subheading text-muted">Enterate de los últimos eventos organizados por el club</h3>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-6 mb-4">
                        <!-- Portfolio item 1-->
                        <div class="portfolio-item">
                            <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal1">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-fluid" src="assets/img/logos/logo_icpc.png" alt="..." style="height: 300px;" />
                            </a>
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading">ICPC</div>
                                <div class="portfolio-caption-subheading text-muted">Participa en el concurso de programación más importante de México y el Mundo</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-4">
                        <!-- Portfolio item 2-->
                        <div class="portfolio-item">
                            <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal2">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-fluid" src="assets/img/logos/logo_faang.png" alt="..." style="height: 300px; width: auto; display: block;  margin-left: auto; margin-right: auto;" />
                            </a>
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading">Curso Internships</div>
                                <div class="portfolio-caption-subheading text-muted">Descubre como trabajar mientras eres estudiante en Facebook, Microsoft, Google ...</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-4">
                        <!-- Portfolio item 3-->
                        <div class="portfolio-item">
                            <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal3">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-fluid" src="assets/img/algorithms.png" alt="..." style="height: 300px;" />
                            </a>
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading">Curso algoritmos</div>
                                <div class="portfolio-caption-subheading text-muted">Aprende a resolver problemas usando lógica y programación</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Facebook -->
        <section class="page-section" id="redes">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Redes sociales</h2>
                <h3 class="section-subheading text-muted">Mantene al tanto a través de nuestras redes sociales</h3>
            </div>
            <div class="container">
                <div class="row text-center">
                    <div class="col">
                        <div class="fb-page" data-href="https://www.facebook.com/CPCFI-page-110095921599860/" data-tabs="timeline" data-width="300" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/CPCFI-page-110095921599860/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/CPCFI-page-110095921599860/">CPCFI-page</a></blockquote></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Youtube -->
        <section class="page-section bg-light" id="youtube">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Youtube </h2>
                <h3 class="section-subheading text-muted">Revisa nuestro contendio en youtube</h3>
            </div>
            <div class="container">
                <div class="row text-center">
                    <div class="col">
                        <iframe width="100%" height="500px" src="https://www.youtube.com/embed/vUrjYBKdIGc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact-->
        <section class="page-section" id="contact">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Contactanos</h2>
                    <h3 class="section-subheading text-muted">Haznos llegar tus dudas, sugerencias o colaboraciones</h3>
                </div>
                <form id="contactForm" action="process_form.php" method="post">
                    <div class="row align-items-stretch mb-5">
                        <div class="col-md-6">
                            <div class="form-group">
                                <!-- Name input-->
                                <input class="form-control" id="name" name="name" type="text" placeholder="Tu nombre *" data-sb-validations="required" />
                                <div class="invalid-feedback" data-sb-feedback="name:required">Se requiere un nombre.</div>
                            </div>
                            <div class="form-group">
                                <!-- Email address input-->
                                <input class="form-control" id="email" name="email" type="email" placeholder="Tu email *" data-sb-validations="required,email" />
                                <div class="invalid-feedback" data-sb-feedback="email:required">Se requiere un email.</div>
                                <div class="invalid-feedback" data-sb-feedback="email:email">El email no es válido.</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-textarea mb-md-0" style="height: 150px;">
                                <!-- Message input-->
                                <textarea class="form-control" id="message" name="message" placeholder="Tu mensaje *" data-sb-validations="required"></textarea>
                                <div class="invalid-feedback" data-sb-feedback="message:required">Un mensaje es requerido.</div>
                            </div>
                        </div>
                    </div>
                    <!-- Submit Button-->
                    <div class="text-center"><button class="btn btn-primary btn-xl text-uppercase" id="submitButton" type="submit">Enviar</button></div>
                </form>
            </div>
        </section>
        <!-- Footer-->
        <div id="footer-placeholder">
        </div>
        <script>
            $(function(){
              $("#footer-placeholder").load("footer.html");
            });
        </script>
        <!-- Portfolio Modals-->
        <!-- Portfolio item 1 modal popup-->
        <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="modal-body">
                                    <!-- Project details-->
                                    <h2 class="text-uppercase">ICPC</h2>
                                    <p class="item-intro text-muted">International Collegiate Programming Contest, el concurso más grande de programación a nivel mundial</p>
                                    <img class="img-fluid d-block mx-auto" src="assets/img/logos/logo_icpc.png" alt="..." style="width:50%;"/>
                                    <p>El International Collegiate Programming Contest es el concurso de programación competitiva con mayor número de participantes a nivel mundial para estudiantes universitarios.</p>
                                    <p>Enterate de como participar, fechas y los recursos de entrenamiento que tenemos para este concurso</p>
                                    <a href="icpc.php#icpc">
                                        <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                            Ir
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Portfolio item 2 modal popup-->
        <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="modal-body">
                                    <!-- Project details-->
                                    <h2 class="text-uppercase">Curso Internships</h2>
                                    <p class="item-intro text-muted">¿Te gustaría trabajar mientras eres estudiante en Facebook, Google o Microsoft?</p>
                                    <img class="img-fluid d-block mx-auto" src="assets/img/logos/logo_faang.png" alt="..." style="width:50%;" />
                                    <p><strong>Atención:</strong> este curso ya fue impartido pero aún puedes consultar el material</p>
                                    <p>En este curso te contamos los secretos del proceso de selección de las empresas más grandes de teconología
                                    </br>Nuestro curso es impartido por alumnos del club que han realizado estancias en Facebook, Google y Microsoft
                                    </p>
                                    <p><strong>Horario:</strong> Tres jueves consecutivos, empezando el  jueves 15 de Agosto de 3:30pm a 4:30pm 
                                    </br><strong>Lugar:</strong> Salón T101 (Sala IBM) en el anexo de la Faculta de Ingeniería </p>
                                    <p><strong>Temario</strong></p>
                                    <ul style="text-align: left;">
                                        <li>
                                            <strong>Internships 101:</strong>
                                            Te explicaremos en que consisten las estancias de verano en estas empresas, la experiencia y el proceso de selección que se sigue
                                        </li>
                                        <li>
                                            <strong>Resume 101:</strong>
                                            Te ayudaremos a elaborar tu resume desde cero, para que puedas aplicar a las empresas que más te interesen
                                        </li>
                                        
                                        <li>
                                        <strong>Entrevistas 101:</strong>
                                            Te enseñaremos como prepararte para las entrevistas de trabajo, y como responder a las preguntas más comunes
                                        </li>
                                    </ul>
                                    <a href="https://drive.google.com/drive/folders/1cf7TtaLGYBnFa5iHamXZi2n3dW10MJa1?usp=sharing">
                                        <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                            Ver material
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Portfolio item 3 modal popup-->
        <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div>
                                    <!-- Project details-->
                                    <h2 class="text-uppercase">Curso Algoritmos</h2>
                                    <p class="item-intro text-muted">¿Te gustaría aprender a resolver problemas usando lógica y programación?</p>
                                    <img class="img-fluid d-block mx-auto" src="assets/img/algorithms.png" alt="..." style="width:50%;" />
                                    <p>Este curso es ideal si te interesaría aprender a resolver problemas de programación competitiva, los cuáles requieren conocimiento teoríco así como creativo
                                    <p><strong>Horario:</strong> Tres jueves consecutivos, empezando el  jueves 15 de Septiembre de 3:30pm a 4:30pm 
                                    </br><strong>Lugar:</strong> Salón T101 (Sala IBM) en el anexo de la Faculta de Ingeniería </p>
                                    <p><strong>Temario</strong></p>
                                    <ul style="text-align: left; list-style-type: none">
                                        <li style="margin-bottom: 10px">
                                            <strong>Algoritmos:</strong>
                                            <ul>
                                                <li>Ordenamiento: merge sort, quick sort, etc</li>
                                                <li>Búsqueda: sliding window, two pointers, binary search, etc</li>
                                                <li>Matemáticas: exponenciación binaria, algoritmo de Euclides, criba de Eratóstenes</li>
                                                <li>Gráficas: depth first search, breadth first search, distancia mínima, componentes conexas, flujo máximo, etc</li>
                                                <li>Programación dinámica</li>
                                            </ul>
                                        </li>
                                        <li>
                                            <strong>Estructuras de datos</strong>
                                            <ul>
                                                <li>Arreglos, diccionarios y sets</li>
                                                <li>Stacks y colas de prioridad</li>
                                                <li>Representación de gráficas: matriz de adjacencia y lista de adjacencia</li>
                                                <li>Linked lists y double linked list</li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                        Inscribirse
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <script>
        $("#contactForm").submit(function(event) 
        {
            /* stop form from submitting normally */
            event.preventDefault();

            /* get some values from elements on the page: */
            var $form = $( this ),
                $submit = $form.find( 'button[type="submit"]' ),
                name_value = $form.find( 'input[name="name"]' ).val(),
                email_value = $form.find( 'input[name="email"]' ).val(),
                message_value = $form.find( 'textarea[name="message"]' ).val(),
                url = $form.attr('action');

            /* Send the data using post */
            var posting = $.post( url, { 
                            name: name_value, 
                            email: email_value, 
                            message: message_value 
                        });

            posting.done(function( data )
            {
                /* Change the button text. */
                console.log(data);
                if(data.includes("success"))
                {
                    $submit.text('Mensaje recibido!');
                }
                else
                {
                    $submit.text('Error, intenta de nuevo');
                }
                /* Disable the button. */
                $submit.attr("disabled", true);
            });
        });
        </script>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>
