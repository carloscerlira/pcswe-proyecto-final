<?php

$hostname = "localhost";
$username = "dbcuenta05";
$password = "Cta.m1sql05";
$dbname = "db05";

$mysqli = new mysqli(hostname: $hostname,
                     username: $username,
                     password: $password,
                     database: $dbname);
                     
if ($mysqli->connect_errno) {
    die("Connection error: " . $mysqli->connect_error);
}

return $mysqli;