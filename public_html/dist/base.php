<?php
    include('connection.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Club de Programación Competitiva de la Facultad de Ingeniería UNAM. Encuentra concursos, material de preparación, cursos y eventos relacionados a programación competitiva" />
        <meta name="author" content="CPCFI" />
        <title>Club de Programación Competitiva de la Facultad de Ingeniería UNAM</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/logos/logo_cpcfi_2.png" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" /> -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <div id="nav-placeholder">
        </div>
        <script>
            $(function(){
                $("#nav-placeholder").load("navbar.html");
            });
        </script>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <div class="masthead-heading text-uppercase">CPCFI</div>
                <div class="masthead-subheading">Club de Programación Competitiva de la Facultad de Ingeniería, UNAM</div>
                <!-- <a class="btn btn-primary btn-xl text-uppercase" href="#services">Tell Me More</a> -->
            </div>
        </header>


        


        <!-- Footer-->
        <div id="footer-placeholder">
        </div>
        <script>
            $(function(){
              $("#footer-placeholder").load("footer.html");
            });
        </script>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>