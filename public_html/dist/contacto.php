<?php
    include('connection.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Club de Programación Competitiva de la Facultad de Ingeniería UNAM. Encuentra concursos, material de preparación, cursos y eventos relacionados a programación competitiva" />
        <meta name="author" content="CPCFI" />
        <title>Club de Programación Competitiva de la Facultad de Ingeniería UNAM</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/logos/logo_cpcfi_2.png" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" /> -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="index.php"><img src="assets/img/logos/logo_letras.png" alt="..." style="width: 130px; height: auto"/></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ms-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                        <li class="nav-item"><a class="nav-link" href="index.php#services">Inicio</a></li>
                        <li class="nav-item"><a class="nav-link" href="acerca.php#about">Acerca de</a></li>
                        <li class="nav-item"><a class="nav-link" href="concursos.php#concurso_cpcfi">Concurso CPCFI 2022</a></li>
                        <li class="nav-item"><a class="nav-link" href="recursos.php#recursos">Recursos</a></li>
                        <li class="nav-item"><a class="nav-link" href="contacto.php#redes">Contacto</a></li>
                        <?php if (isset($user)): ?>
                            <li class="nav-item"><a class="nav-link" href="logout.php"><?= htmlspecialchars($user["name"])?></a></li>
                        <?php else: ?>
                            <li class="nav-item"><a class="nav-link" href="login.php">Login</a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <img src="assets/img/logos/logo_cpcfi_3.png" style="width: 300px;"></img>                
                <!-- <div class="masthead-heading text-uppercase">CPCFI</div> -->
                <div class="masthead-subheading">Club de Programación Competitiva de la Facultad de Ingeniería, UNAM</div>
                <!-- <a class="btn btn-primary btn-xl text-uppercase" href="#services">Tell Me More</a> -->
            </div>
        </header>
        
        <!-- Redes sociales -->
        <section class="page-section" id="redes">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Redes sociales</h2>
                    <h3 class="section-subheading text-muted">Siguenos en nuestras redes sociales para mantenerte actualizado</h3>
                </div>
                <div class="row text-center">
                    <div class="col-md-4">
                        <a href="https://www.youtube.com/channel/UCbO49RzpwIa5_I3x-EDlpug">
                            <!-- <span class="fa-stack fa-4x">
                                <i class="fas fa-circle fa-stack-2x" style="color: #ff3a86;"></i>
                                <i class="fas fa-building-columns fa-stack-1x fa-inverse"></i>
                            </span> -->
                            <img src="assets/img/logos/logo_yt.png" style="width: auto; height: 200px;">
                        </a>
                        <h4 class="my-3">Youtube</h4>
                        <!-- <p class="text-muted">Encuentra algoritmos y estructuras de datos explicados desde cero</p> -->
                    </div>
                    <div class="col-md-4">
                        <a href="https://www.facebook.com/CPCFI-page-110095921599860">
                            <!-- <span class="fa-stack fa-4x">
                                <i class="fas fa-circle fa-stack-2x" style="color: #ffc83b;"></i>
                                <i class="fas fa-trophy fa-stack-1x fa-inverse"></i>
                            </span> -->
                            <img src="assets/img/logos/logo_fb.png" style="width: auto; height: 200px;">
                        </a>
                        <h4 class="my-3">Facebook</h4>
                        <!-- <p class="text-muted">Participa en los concursos anuales organizados por el club</p> -->
                    </div>
                    <div class="col-md-4">
                        <a href="https://discord.gg/kMM89VbQ7M">
                            <!-- <span class="fa-stack fa-4x">
                                <i class="fas fa-circle fa-stack-2x" style="color: #bb1534;"></i>
                                <i class="fas fa-at fa-stack-1x fa-inverse"></i>
                            </span> -->
                            <img src="assets/img/logos/logo_discord.png" style="width: auto; height: 200px;">
                        </a>
                        <h4 class="my-3">Discord</h4>
                        <!-- <p class="text-muted">Haznos llegar tus dudas, propuestas o colaboraciones</p> -->
                    </div>
                </div>
            </div>
        </section>

        <!-- Portfolio Grid-->
        <section class="page-section bg-light" id="ubicacion">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Ubicación</h2>
                    <h3 class="section-subheading text-muted">Visitanos en la Anexo de la Faculta de Ingeniería, UNAM. Nos ubicamos en el salón T101 (Sala de IBM) de Lunes a Jueves de 3:30 pm a 5:00 pm</h3>
                </div>
                <div class="row text-center">
                    <div class="col">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d959.5369167410029!2d-99.18066054480512!3d19.327784271397007!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbece8754e782f3df!2zMTnCsDE5JzQxLjQiTiA5OcKwMTAnNTIuNyJX!5e0!3m2!1sen!2sus!4v1662747411707!5m2!1sen!2sus" width="50%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </section>

        <!-- Contact-->
        <section class="page-section" id="contact">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Contactanos</h2>
                    <h3 class="section-subheading text-muted">Haznos llegar tus dudas, sugerencias o colaboraciones</h3>
                </div>
                <form id="contactForm" action="process_form.php" method="post">
                    <div class="row align-items-stretch mb-5">
                        <div class="col-md-6">
                            <div class="form-group">
                                <!-- Name input-->
                                <input class="form-control" id="name" name="name" type="text" placeholder="Tu nombre *" data-sb-validations="required" />
                                <div class="invalid-feedback" data-sb-feedback="name:required">Se requiere un nombre.</div>
                            </div>
                            <div class="form-group">
                                <!-- Email address input-->
                                <input class="form-control" id="email" name="email" type="email" placeholder="Tu email *" data-sb-validations="required,email" />
                                <div class="invalid-feedback" data-sb-feedback="email:required">Se requiere un email.</div>
                                <div class="invalid-feedback" data-sb-feedback="email:email">El email no es válido.</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-textarea mb-md-0" style="height: 150px;">
                                <!-- Message input-->
                                <textarea class="form-control" id="message" name="message" placeholder="Tu mensaje *" data-sb-validations="required"></textarea>
                                <div class="invalid-feedback" data-sb-feedback="message:required">Un mensaje es requerido.</div>
                            </div>
                        </div>
                    </div>
                    <!-- Submit Button-->
                    <div class="text-center"><button class="btn btn-primary btn-xl text-uppercase" id="submitButton" type="submit">Enviar</button></div>
                </form>
            </div>
        </section>

        <!-- Footer-->
        <div id="footer-placeholder">
        </div>
        <script>
            $(function(){
              $("#footer-placeholder").load("footer.html");
            });
        </script>

        
        <script>
            $("#contactForm").submit(function(event) 
            {
                /* stop form from submitting normally */
                event.preventDefault();

                /* get some values from elements on the page: */
                var $form = $( this ),
                    $submit = $form.find( 'button[type="submit"]' ),
                    name_value = $form.find( 'input[name="name"]' ).val(),
                    email_value = $form.find( 'input[name="email"]' ).val(),
                    message_value = $form.find( 'textarea[name="message"]' ).val(),
                    url = $form.attr('action');

                /* Send the data using post */
                var posting = $.post( url, { 
                                name: name_value, 
                                email: email_value, 
                                message: message_value 
                            });

                posting.done(function( data )
                {
                    /* Change the button text. */
                    console.log(data);
                    if(data.includes("success"))
                    {
                        $submit.text('Mensaje recibido!');
                    }
                    else
                    {
                        $submit.text('Error, intenta de nuevo');
                    }
                    /* Disable the button. */
                    $submit.attr("disabled", true);
                });
            });
        </script>


        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>