<?php
    include('connection.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Club de Programación Competitiva de la Facultad de Ingeniería UNAM. Encuentra concursos, material de preparación, cursos y eventos relacionados a programación competitiva" />
        <meta name="author" content="CPCFI" />
        <title>Club de Programación Competitiva de la Facultad de Ingeniería UNAM</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/logos/logo_cpcfi_2.png" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" /> -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="index.php"><img src="assets/img/logos/logo_letras.png" alt="..." style="width: 130px; height: auto"/></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ms-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                        <li class="nav-item"><a class="nav-link" href="index.php#services">Inicio</a></li>
                        <li class="nav-item"><a class="nav-link" href="acerca.php#about">Acerca de</a></li>
                        <li class="nav-item"><a class="nav-link" href="concursos.php#concurso_cpcfi">Concurso CPCFI 2022</a></li>
                        <li class="nav-item"><a class="nav-link" href="recursos.php#recursos">Recursos</a></li>
                        <li class="nav-item"><a class="nav-link" href="contacto.php#redes">Contacto</a></li>
                        <?php if (isset($user)): ?>
                            <li class="nav-item"><a class="nav-link" href="logout.php"><?= htmlspecialchars($user["name"])?></a></li>
                        <?php else: ?>
                            <li class="nav-item"><a class="nav-link" href="login.php">Login</a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <img src="assets/img/logos/logo_cpcfi_3.png" style="width: 300px;"></img>                
                <!-- <div class="masthead-heading text-uppercase">CPCFI</div> -->
                <div class="masthead-subheading">Club de Programación Competitiva de la Facultad de Ingeniería, UNAM</div>
                <!-- <a class="btn btn-primary btn-xl text-uppercase" href="#services">Tell Me More</a> -->
            </div>
        </header>
        <!-- Body -->
        <section class="page-section" id="icpc">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">International Collegiate Programming Contest</h2>
                <!-- <img class="img-fluid d-block mx-auto" src="assets/img/icpc2.jpg" alt="..."/> -->
            </br>
        </div>
        <div class="container">
            <div class="row text-center">
                <div class="col" style="text-align: left;">
                    <p>
                        La Competición Internacional Universitaria de Programación (en inglés International Collegiate Programming Contest, abreviado ICPC) es una competición anual de programación y algorítmica entre universidades de todo el mundo, donde prima el trabajo en equipo, el análisis de problemas y el desarrollo rápido de software. Tiene su sede en la Universidad de Baylor y está liderada por su Director Ejecutivo y profesor de esta universidad, el Dr. William B. Poucher. La ICPC lleva a cabo concursos regionales autónomos que cubren los seis continentes y culminan en una final mundial cada año. En 2018, la participación del ICPC incluyó a 52 709 estudiantes de 3 233 universidades en 110 países.
                    </p>
                    <iframe width="100%" height="500px" src="https://www.youtube.com/embed/D66ps6Trc00" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <figcaption style="text-align: center; margin-bottom: 20px"> ICPC World Finals 2022, ITMO University, Russia </figcaption>
                    </br>
                    <div>
                        <h3 class="section-heading text-uppercase">Misión</h2>
                        <p>
                            La Competición Internacional Universitaria de Programación es una actividad extracurricular y deporte de programación competitiva para estudiantes de universidades de todo el mundo. Las competiciones de ICPC brindan a los estudiantes aventajados la oportunidad de interactuar, demostrar y mejorar sus habilidades de trabajo en equipo, programación y resolución de problemas. La ICPC es una plataforma global para la academia, la industria y la comunidad para destacar y elevar las aspiraciones de la próxima generación de profesionales de la computación en su búsqueda de la excelencia.
                        </p>
                        </br>
                    </div>
                    <div>
                        <h3 class="section-heading text-uppercase">Historia</h2>
                        <p>
                            La ICPC es una competición que se inició en la Universidad A&M de Texas en 1970. Pasó a ser una competición con varias rondas clasificatorias en 1977 y la final mundial se organizó en colaboración con la ACM Computer Science Conference.
                        <p>
                        <p>
                            De 1977 a 1989, compitieron principalmente equipos de Estados Unidos y Canadá. La sede central está ubicada desde 1989 en la Universidad de Baylor y las competiciones regionales se ubican en universidades de todo el mundo, bajo el auspicio de ACM y la colaboración de grandes empresas de la industria informática. La ICPC ha ido aumentando en número de participantes y países, por lo que ahora es una competición mundial con equipos de 103 países (en 2016).
                        </p>
                        <p>
                            Desde 1997 el principal patrocinador es IBM y la participación en la competición ha aumentado enormemente. En 1997 participaron 840 equipos de 560 universidades. En 2016 participaron 46381 estudiantes de 2948 universidades de todo el mundo.1​ El número de equipos aumenta entre un 10% y un 20% cada año.
                        </p>
                        </br>
                    </div>
                    <div>
                        <h3 class="section-heading text-uppercase">Reglas de la competición</h2>
                        <p>
                            El ICPC es una competición por equipos. Las reglas actuales estipulan que cada equipo ha de tener exactamente 3 miembros. Los miembros han de ser estudiantes universitarios, que hayan estudiado menos de 5 años en la universidad antes del concurso. Los estudiantes que hayan competido en dos finales mundiales (World Finals) o cinco competiciones regionales no pueden participar otra vez.
                        <p>
                            Durante la competición, los equipos tienen alrededor de 5 horas para resolver entre 8 y 15 problemas (lo normal es 8 para las competiciones regionales y 12 para la final). Se deben programar las soluciones con C, C++, Java, Ada, Python o Kotlin2​3​ (aunque no está garantizado que todos los problemas sean solucionables usando cualquier lenguaje, el ICPC establece que los jueces habrán resuelto todos los problemas en Java y C++, tanto para los regionales como para la final mundial). Los programas enviados por los equipos se compilan y ejecutan con unos ciertos datos de entrada, si el programa falla al calcular la solución, el equipo es notificado del error y pueden enviar nuevamente el programa o probar con otros problemas.
                        </p>
                        <p>
                            El ganador es el equipo que resuelve más problemas. Si hay equipos empatadas con el mismo número de problemas resueltos, el orden de clasificación se calcula a partir de los que han tardado menos en resolver los problemas.
                        </p>
                        <p>
                            Ejemplo: si un equipo A ha enviado sus soluciones para 2 problemas los 60 y 120 minutos desde el inicio del concurso, y otro equipo B lo ha hecho a los 80 y 90 minutos. El desempate entre ambos equipos se haría mirando los tiempos, para el equipo A: 60+120 = 180 minutos. Para el equipo B: 80+90 = 170 minutos. El equipo B ganaría.
                        </p>
                        <p>
                            El tiempo que se toma para los desempates es el tiempo que ha pasado desde el inicio del concurso más 20 minutos por cada solución incorrecta enviada. En el ejemplo anterior, si el equipo A hubiera enviado 2 soluciones incorrectas para su primer problema, su tiempo final sería: 20+20+60+120 = 220.
                        </p>
                        <p>
                            El ICPC se diferencia de otras competiciones de programación (por ejemplo la IOI) en que suele tener un gran número de problemas (8 o más para resolver en 5 horas) y que es una competición por equipos con un solo ordenador. Es necesario un buen entendimiento entre los miembros de un equipo para conseguir la victoria.
                        </p>
                        </br>
                    </div>
                    <div>
                        <h3 class="section-heading text-uppercase">Competiciones locales, regionales y final mundial</h2>
                        <p>
                            La competición tiene distintas fases clasificatorias. Algunas universidades tienen competiciones locales para elegir a los componentes de los equipos. Cada universidad puede mandar un máximo de equipos de 3 personas a la fase regional que puede variar en dependencia de la región. Las competiciones locales son opcionales y las organiza cada universidad como estime conveniente. Algunas universidades optan por seleccionar a los alumnos con notas más altas o a los que muestran más interés.
                        <p>
                            Los equipos de cada universidad participan en la fase regional, con otros equipos de universidades próximas geográficamente. Hay más de 30 regiones en todo el mundo. Algunas regiones agrupan distintos países (por ejemplo la SWERC incluye a España, Portugal, Francia y otros países europeos), otras son un solo país (la región de Brasil) y otras son sólo parte un país (Estados Unidos está dividido en varias regiones). Los mejor clasificados en cada competición regional participarán en la final mundial. Cada región envía a la final un cierto número de equipos, no pudiendo haber más de un equipo de una misma universidad.
                        </p>
                        <p></p>
                            La final mundial se celebra cada año en una ciudad distinta, y congrega a los equipos ganadores de todas las competiciones regionales.
                        </p>
                        </br>
                    </div>
                    <div>
                        <h3 class="section-heading text-uppercase">Inscripción</h2>
                        <p>
                            Para inscribirse en México debes visitar la página de ITESO, la universidad de México encargada de realizar este evento en el país. Así mismo realizaremos eventos de difusión en la universidad y publicaremos en nuestra página una vez se publique la convocatoria.
                        <p>
                        <a href="https://blogs.iteso.mx/acm/">
                            <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                <!-- <i class="fas fa-xmark me-1"></i> -->
                                Ir
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer-->
        <div id="footer-placeholder">
        </div>
        <script>
            $(function(){
              $("#footer-placeholder").load("footer.html");
            });
        </script>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>