<?php
    include('connection.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Club de Programación Competitiva de la Facultad de Ingeniería UNAM. Encuentra concursos, material de preparación, cursos y eventos relacionados a programación competitiva" />
        <meta name="author" content="CPCFI" />
        <title>Club de Programación Competitiva de la Facultad de Ingeniería UNAM</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/logos/logo_cpcfi_2.png" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" /> -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <style>
            li.menu-item {
                margin:0 0 30px 0;   
            }
        </style>
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="index.php"><img src="assets/img/logos/logo_letras.png" alt="..." style="width: 130px; height: auto"/></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ms-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                        <li class="nav-item"><a class="nav-link" href="index.php#services">Inicio</a></li>
                        <li class="nav-item"><a class="nav-link" href="acerca.php#about">Acerca de</a></li>
                        <li class="nav-item"><a class="nav-link" href="concursos.php#concurso_cpcfi">Concurso CPCFI 2022</a></li>
                        <li class="nav-item"><a class="nav-link" href="recursos.php#recursos">Recursos</a></li>
                        <li class="nav-item"><a class="nav-link" href="contacto.php#redes">Contacto</a></li>
                        <?php if (isset($user)): ?>
                            <li class="nav-item"><a class="nav-link" href="logout.php"><?= htmlspecialchars($user["name"])?></a></li>
                        <?php else: ?>
                            <li class="nav-item"><a class="nav-link" href="login.php">Login</a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <img src="assets/img/logos/logo_cpcfi_3.png" style="width: 300px;"></img>                
                <!-- <div class="masthead-heading text-uppercase">CPCFI</div> -->
                <div class="masthead-subheading">Club de Programación Competitiva de la Facultad de Ingeniería, UNAM</div>
                <!-- <a class="btn btn-primary btn-xl text-uppercase" href="#services">Tell Me More</a> -->
            </div>
        </header>
        
        <section class="page-section" id="recursos">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Recursos</h2>
                    <!-- <h3 class="section-subheading text-muted">Descubre qué es el CPCFI</h3> -->
                </div>
                <div class="container" style="margin-top: 30px;">
                    <div class="row text-center">
                        <div class="col" style="text-align: left;">
                            <ul id="recursos-list" style="list-style-type: none;">
                                <li style="margin-bottom: 10px;">
                                    <h3>Algebra</h3>
                                    <ul>
                                        <li>
                                            <a href="binary_exp.php#begin">Exponenciación Binaria</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Algoritmo Euclidiano para el cómputo del divisor común mayor
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Algoritmo Euclidiano Extendido
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Números de Fibonacci
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Ecuaciones Diofánticas Lineales
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li style="margin-bottom: 10px;">
                                    <h3>Estructuras de datos</h3>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Disjoint Set Union
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Fenwick Tree
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Sqrt Decomposition
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Segment Tree
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Treap
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Sqrt Tree
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Randomized Heap
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li style="margin-bottom: 10px;">
                                    <h3>Programación dinámica</h3>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Divide y venceras
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                               La optimización de Knuth
                                           </a> 
                                        </li>
                                    </ul>
                                </li>
                                <li style="margin-bottom: 10px;">
                                    <h3>Procesamiento de Cadenas</h3>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                String hashing
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Rabin-Karp para match de strings
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                La función Z
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li style="margin-bottom: 10px;">
                                    <h3>Combinatoria</h3>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Fundamentos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Técnicas
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li style="margin-bottom: 10px;">
                                    <h3>Métodos Numéricos</h3>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Búsqueda
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Integración
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li style="margin-bottom: 10px;">
                                    <h3>Geometría</h3>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Operaciones elementales
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Polígonos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Cápsula convexa
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Línea deslizante
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Diversos
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li style="margin-bottom: 10px;">
                                    <h3>Grafos</h3>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Recorrido de grafos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Componentes conexos, puentes, puntos articulados
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Camino mínimo de única fuente
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Camino mínimo todos con todos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Árboles generadores
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Ciclos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Ancestro común más bajo
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Flujo y problemas relacionados
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Emparejamiento y problemas relacionados
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Problemas diversos
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li style="margin-bottom: 10px;">
                                    <h3>Diversos</h3>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                Teoría de juegos
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <p>
                                Traducción al español de <a href="http://e-maxx.ru/algo/">e-maxx.ru</a> y <a href="https://cp-algorithms.com/">cp-algorithms.com</a> 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer-->
        <div id="footer-placeholder">
        </div>
        <script>
            $(function(){
              $("#footer-placeholder").load("footer.html");
            });
        </script>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>