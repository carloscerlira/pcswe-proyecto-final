# Manual de instalación
## Requisitos:
1. Git
2. Cuenta en GitLab
3. XAMPP

## Instalación:
1. Inicializar un servidor Apache y MySQL en XAMPP
2. Entrar al directorio htdocs de XAMPP y clonar repositorio
```
git clone https://gitlab.com/carloscerlira/pcswe-proyecto-final.git 
```
3. Conectarse a base mysql, correr script create_database.sql el cuál creara la base y las tablas 
4. Configurar el archivo dist/database.php con los valores correctos (cambiar username y password por el correcto)
```
<?php

$hostname = "localhost";
$username = "dbcuenta05";
$password = "Cta.m1sql05";
$dbname = "db05";

$mysqli = new mysqli(hostname: $hostname,
                     username: $username,
                     password: $password,
                     database: $dbname);
                     
if ($mysqli->connect_errno) {
    die("Connection error: " . $mysqli->connect_error);
}

return $mysqli;
```
5. Listo, la página debería funcionar correctamente, para probar puedes abrir el archivo http://localhost/pcswe-proyecto-final/public_html/dist/index.php en un navegador web.